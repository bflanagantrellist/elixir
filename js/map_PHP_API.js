function initialize() {
	window.markers = [];
	window.MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';
	window.hostnameRegexp = new RegExp('^https?://.+?/');

	window.map = new google.maps.Map(document.getElementById("map-canvas"), {
		center: new google.maps.LatLng(0, 0),
		zoom: 1
	});

	window.infoWindow = new google.maps.InfoWindow();
	google.maps.event.addListener(map, 'click', function(){
		infoWindow.close();
	});

	window.geocoder = new google.maps.Geocoder();

	window.autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')));
	window.places = new google.maps.places.PlacesService(map);
	google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);

	// Attach a listener to the submit button.
	$('#submitbut').on('click', function(e){
		if(e){
			e.preventDefault();
			e.stopPropagation();
		}
		search();
	});

	// Attach listeners to mobile radius buttons.
	$('.findAStore .radiusSelector').on('click', function(e){
		if(e){
			e.preventDefault();
			e.stopPropagation();
		}

		var radius = $(this).attr('name').replace('within', '');
		search(radius);
	});
}

google.maps.event.addDomListener(window, 'load', initialize);

function onPlaceChanged() {
	var place = autocomplete.getPlace();
	if (place.geometry) {
		map.panTo(place.geometry.location);
		map.setZoom(15);
		search();
	} else {
		document.getElementById('address').placeholder = 'Zip, City or State';
	}
}

function search(radius) {
	// Get the geocoordinates from the address entered in the search field.
	var address = document.getElementById('address').value;
	geocoder.geocode({address: address}, function(results, status){
		if(status == google.maps.GeocoderStatus.OK){
			findStores(results[0].geometry.location, radius);
		} else {
			alert(address + ' not found');
		}
	});
}

function downloadUrl(url,callback) {
	$.ajax({
	   	type: 'GET',
	    url: url,
	    contentType: "application/json",
	    dataType: 'jsonp',
	    success: function(response){
	    	callback(response);
	    }
	});	
	var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest;
}

function findStores(center, radius) {
	clearLocations();

	var radius = radius || 300;
	var searchUrl = 'http://elixir.trellist-dev.com/api/locations_data.php?callback=?&lat=' + center.lat() + '&lng=' + center.lng() + '&radius=' + radius;
	
	downloadUrl(searchUrl, function(data) {
		var bounds = new google.maps.LatLngBounds();

		$('.findAStore #results').empty();

		$(data).each(function(i){
			if(i < 5){
				// Set up the map marker.
				var latlng = new google.maps.LatLng(
					parseFloat(this.lat),
					parseFloat(this.lng)
				);

				createMarker(latlng, this.name, this.address);
				bounds.extend(latlng);

				// Add to the results list.
				var node = $('<div class="location"></div>');

				var name = $('<div class="name">' + this.name + '</div>');
				var address = $('<div class="address">' + this.address + '</div>');
				var address2 = $('div class="address">' + this.city + ' ' + this.state + ' ' + this.zip + '</div>');
				var phone = $('<div class="phone"><label>Phone</label>' + this.phone + '</div>');
				var distance = $('<div class="distance"><label>Distance</label>' + parseFloat(this.distance).toFixed(2) + ' miles</div>');
				var website = $('<div class="website"><a href="' + this.website + '" target="_blank">'+ this.website + '</a></div>');
				var directionsLink = $('<div class="directions"><a href="https://maps.google.com/maps?daddr=' + this.address + '" target="_blank">Directions</a></div>')
				
				node.append(name)
					.append(address)
					.append(phone)
					.append(distance)
					//.append(website)
					.append(directionsLink);

				$('.findAStore #results').append(node);
			}
		});

		// If no results were found, set the map center to the location entered.
		if(data.length == 0){
			var latlng = new google.maps.LatLng(
				parseFloat(center.lat()),
				parseFloat(center.lng())
			);
			bounds.extend(latlng);

			$('.findAStore #results').append($('<div class="name">No stores found.</div>'));
		}

		map.fitBounds(bounds);
	});
}

function createMarker(latlng, name, address) {
	var html = "<b>" + name + "</b> <br/>" + address;

	var marker = new google.maps.Marker({
		map: map,
		position: latlng
	});

	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(html);
		infoWindow.open(map, marker);
	});
	markers.push(marker);
}

function clearLocations() {
  	infoWindow.close();
  	for (var i = 0; i < markers.length; i++) {
    	markers[i].setMap(null);
  	}
  	markers.length = 0;

  	$('.findAStore #results').empty();
}