 $(document).ready(function(){
 	$('div#nav ul.nav a').click(function(event){
 		if(!$(this).hasClass('homeLink')){
	 		event.preventDefault();
 			var $this = $(this),
 				section = $this.attr("href");

	 		//get the top offset of the target anchor
    	    var target_offset = $("."+section).offset();
        	var target_top = target_offset.top;

 			$('html, body').animate({scrollTop:target_top},  'slow');
 		}
 	});

 	var $p = $('div.show-more'),
 		innerHeight = $p.removeClass('show-more').height();
 	
 	$p.addClass("show-more");
 	
 	$('div.read-more').click(function(){
 		var rm = $('div.read-more');
 		
 		$p.animate({
 			height:(($p.height() == 228)? innerHeight : "248px")
 		},{
 			queue:false,
 			duration:500,
 			complete:function(){
 				rm.text(($p.height() == 228)? 'Read More':'Read Less');
 			}
 		});
 		
		return false;
	});
});