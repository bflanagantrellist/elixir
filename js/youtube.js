// Setup a reference to the YouTube player object.
function onYouTubePlayerAPIReady(){
	window.player = new YT.Player('player');
}

// Attach a listener to stop playback if the user closes the lightbox.
$('button.close-modal').click(function(){
	player.stopVideo();
});