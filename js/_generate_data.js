$(function(){
	// Firing up a map object (so I can use the geocoding API without having to write a JSONP routine).
	window.map = new google.maps.Map(document.getElementById("map-canvas"), {
		center: new google.maps.LatLng(0, 0),
		zoom: 1
	});
	window.geocoder = new google.maps.Geocoder();

	// Open up the csv file.
	$.ajax({
		type: 'GET',
		url: 'csv/addresses.csv',
		dataType: 'text',
		success: function(allText) {
			// Setup the number of columns.
			var record_num = 6;

			// Extract the data into an array.
			var allTextLines = allText.split(/\r\n|\n|\r/);

			// The headings are the first element of the array.
			var headings = allTextLines[0].split(',').splice(0, record_num);

			// Remove the headings element from the array.
			allTextLines.splice(0, 1);
				
			// Set up an array the records.
			var lines = [];

			// Convert each element into an object.
			$(allTextLines).each(function(){
				// Split the string up into an array.
				var raw_location = this.split(',');

				// Set up an object.
				var location = {};

				for(var j=0; j<record_num; j++){
					location[headings[j].toLowerCase()] = raw_location[j];
				}

				lines.push(location);
			});

			// Normally, I'd include the routine to extent the element properties within the one loop
			// But Google has a query limiter in place.  It seems to be 11 at a time.

			var leftToProcess = lines.length;
			var failed = [];
			var success = [];

			window.getGeoCodes = function(){
				// Set up some tracking counters.
				var numberToTry = 10;
				var processedCount = 0;

				// Iterate over the array.
				$(lines).each(function(){
					var location = this;

					// Restrict the attempt (so we don't blow out our query quota).
					if(processedCount < numberToTry && !location.lat){
						processedCount++;

						// Retrieve the geocodes (from google ceocode api)
						var address = location.street + ' ' + location.city + ', ' + location.state + ' ' + location.zip;
						console.log('attempting: ' + address);

						geocoder.geocode({
							address: address
						}, function(results, status){
							if(status == google.maps.GeocoderStatus.OK){
								leftToProcess--;
								location.lat = results[0].geometry.location.lat();
								location.lng = results[0].geometry.location.lng();
							} else {
								if(status != 'OVER_QUERY_LIMIT'){
									// Prevent the system from repeatedly trying an address it can't resolve.
									console.log(address + ' failed');
									console.log(status);
									leftToProcess--;
								}
							}
						});
					}
				});
			}

			var loop = setInterval(function(){
				if(leftToProcess > 0){
					console.log('---------   ' + leftToProcess + ' remaining   ---------');
					getGeoCodes();	
				} else {
					clearInterval(loop);
					console.log(JSON.stringify(lines));
				}
			}, 5000);
		}
	});
});