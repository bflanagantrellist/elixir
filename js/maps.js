
		var map = null;
		var home = null;
		var locale = "en_US";
		var accountType = "Retailer";
		var triggerRequest = "";
		var requestedAddress="";
		var addressNotFoundText="Your address could not be found";
		var directionsText='Directions';
		var detailsText='Details';
		var findingStoreText='Finding stores, please wait...';
		var distanceText='Distance';
		var nextText='Next';
		var previousText='Previous';
		var phoneText='Phone';
		var faxText='Fax';
		var routeErrorText='ERROR: Route could not be made:';

		var stores = [];
		var storeMarkers = [];
		var lastPage = 0;
		// Directions control
		var dirControl; // JQuery object representing directionsControl div.
		var travelOptions = null; //Container to store current travel options for the current route.
		// Google Maps
		var geocoder;
		var directionDisplay;
		var directionsService = new google.maps.DirectionsService();
		var infoWin;
		// Autocomplete starts here
		function initialize() {    
		    var input = document.getElementById('address');
		    var autocomplete = new google.maps.places.Autocomplete(input);
		    google.maps.event.addListener(autocomplete, 'place_changed', function() {
		    	if(home)
		    		clearMap();	
		    	var address=$("#address").val();
		    	geocoder.geocode( { 'address': address}, GeocodeCallback);	
		    });
		}

		google.maps.event.addDomListener(window, 'load', initialize);
		//Autocomplete ends here
		//Paging options
		var displayLimit = 5; // Number of stores to be displayed at once
		var pageLimit = 5; // Number of pages to be displayed at once in the pagination list.
		/**
		 * Initialize map
		 */
		$(document).ready(function() {
			var latlng = new google.maps.LatLng(0, 0);
		    var myOptions = {
		      zoom: 1,
		      center: latlng,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		    infoWin = new google.maps.InfoWindow();
		    google.maps.event.addListener(map, 'click', function(){infoWin.close();});
		    // Init. Geocoder
		    geocoder = new google.maps.Geocoder();
		    // Init. Renderer
		    var renderOptions = {
		    	map: map,
		    	panel: document.getElementById("dirPanel"),
		    	draggable: true,
		    	suppressMarkers: true
		    };
		    directionsDisplay = new google.maps.DirectionsRenderer(renderOptions);
		    dirControl = $("#directionsControl");
		    
		    $("#horizontalForm").submit(function(){
		    	if(home)
		    		clearMap();
		    	//var address = $("#address").val();
			var address=$("#address").val();
		    	geocoder.geocode( { 'address': address}, GeocodeCallback);
		    });

		    if(triggerRequest!=null & triggerRequest!=""){
		   	 
		    	$('#address').val(requestedAddress);
		    	//if($.trim(requestedAddress) == "" && $.trim(accountType)=="D")
		    	//	requestedAddress = $("#country").val();
		    	geocoder.geocode( { 'address': requestedAddress}, GeocodeCallback);
		     }

		});

		/**
		 * If location found, set home to result and query database for nearby stores.
		 */
		function GeocodeCallback(results, status,address) {
			//results = pruneCountries(results, $("#country").val());
			$("#modules").children().hide(); // hide all modules initially
			if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
				if(results.length == 1) {
					home = createMarker(results[0].formatted_address, results[0].geometry.location);
					$("#destA").val(results[0].formatted_address);
					/*
					* Search for nearby stores
					*/
			        var start_lat = results[0].geometry.location.lat().toString().substr(0, 12);
			        var start_lng = results[0].geometry.location.lng().toString().substr(0, 12);
			        var country = getCountryFromResult(results[0]);//$("#country").val();
					var searchFormBean = {
							country:country, 
							latitude:start_lat, 
							longitude:start_lng,
							accountType:accountType
						};
					$("#statusModule").html(findingStoreText);
					$("#statusModule").show();
					$.postJSON("http://www.elixirstrings.com:80/locator/display-elixir-stores.json?", searchFormBean, handleStoreData);
				} else {
					var ul = $("#dymResults");
					ul.empty();
					var clickFunc = function(li, curr) {
						li.click(function(){geocoder.geocode( { 'address': curr}, GeocodeCallback2);});
					};
					for(var i = 0; i < results.length; i++) {
						var curr = results[i].formatted_address;
						var li = $("<li style='cursor:pointer' >" + curr + "</li>");
						clickFunc(li, curr);
						ul.append(li);
					}
					$("#dymModule").show();
				}
			} else {
				$("#statusModule").html(addressNotFoundText);
				$("#statusModule").show();
				map.setOptions({zoom:1, center:new google.maps.LatLng(0,0)});
			}		
		}
		/*
		 * call this after did you mean in order to send a request for the first selection or else 
			 infinite loop will occur for the result entered  e.g Deutschland München..
		 */

		 function GeocodeCallback2(results, status) {
		 	//results = pruneCountries(results, $("#country").val());
		 	$("#modules").children().hide(); // hide all modules initially
		 	if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
		 		if(results.length >=1) {
		 			home = createMarker(results[0].formatted_address, results[0].geometry.location);
		 			$("#destA").val(results[0].formatted_address);
		 			/*
		 			* Search for nearby stores
		 			*/
		 	        var start_lat = results[0].geometry.location.lat().toString().substr(0, 12);
		 	        var start_lng = results[0].geometry.location.lng().toString().substr(0, 12);
		 	        var country = getCountryFromResult(results[0]);//$("#country").val();
		 			var searchFormBean = {
		 					country:country, 
		 					latitude:start_lat, 
		 					longitude:start_lng,
		 					accountType:accountType
		 				};
		 			$("#statusModule").html(findingStoreText);
		 			$("#statusModule").show();
		 			$.postJSON("http://www.elixirstrings.com:80/locator/display-elixir-stores.json?", searchFormBean, handleStoreData);
		 		}
		 	} else {
		 		$("#statusModule").html(addressNotFoundText);
		 		$("#statusModule").show();
		 		map.setOptions({zoom:1, center:new google.maps.LatLng(0,0)});
		 	}		
		 }
		/**
		 * Deletes elements from a geocode results array which are not from the selected country.
		 * As the Google Maps only offers a region bias functionality for countries, this
		 * ensures no results other than ones in the specified country are returned.
		 */
		function pruneCountries(results, selectedCountry) {
			selectedCountry = selectedCountry.toUpperCase();
			return $.grep(results, function(location, index) {
				for(var i = location.address_components.length-1; i >= 0; i--) {
					if(location.address_components[i].types[0] == "country") {
						if(location.address_components[i].short_name == selectedCountry)
							return true;
						else
							return false;
					}
				}
				return false;
			});
		}
		/**
		 * Cache store information in stores[] and show stores on map and sidebar.
		 */
		function handleStoreData(data) {
			stores = [];
			$("#statusModule").hide();
			$.each(data, function() {
				var store = {
					lat: this.latitude,
					lng: this.longitude,
					title: this.location_name,
					street: this.street_address1,
					street2: this.street_address2,
					state:this.state,
					city: this.city,
					postalcode: this.postalcode,
					country: this.country,
					distance: (locale == "en_US" ||locale == "en_GB" )? kilosToMiles(this.distance) : this.distance,			
					phone: this.phone,
					fax: this.fax,
					email: this.email,
					url: this.url
				};
				stores.push(store);
			});
			if(stores.length == 0) {
				$("#statusModule").html(addressNotFoundText);
				$("#statusModule").show();
				map.setOptions({zoom:4, center:home.getPosition()});
			} else {
				$("#resultsModule").show();
				lastPage = Math.ceil(stores.length/displayLimit);
				showStoresTab();
				listStores(0);
			}
		}
		/**
		 * Display 'n' stores on the map and in the sidebar starting at the 'start' index of the 'stores' array, clearing previously displayed stores,
		 * where n = displayLimit.
		 */
		function listStores(start) {
			clearStoreMarkers();
			clearRoute();
			var htmlContent = "";
			var limit = start + displayLimit;
			if(limit > stores.length)
				limit = stores.length;
			for(var i = start, marker = 0; i < limit; i++, marker++) {
				var curr = stores[i];
				var street2=curr.street2!=null?curr.street2+'<br/>':'';
				var state=curr.state!=null?curr.state+' ':'';
				

				var postcodeVar=curr.postalcode!=null?curr.postalcode+' ':'';
				var fullAddress ="";
		              var selCountry=curr.country;
				//scandinavian format
		              if(selCountry =="NO" ||selCountry =="SE" ||selCountry =="FI" ||selCountry =="DK"||selCountry =="DE"||selCountry =="AT"){				
					 fullAddress = curr.street + "<br/>"+street2  + postcodeVar + curr.city + " " + state;
		               }
				else
				fullAddress = curr.street + "<br/>"+street2 + curr.city + " " + state + postcodeVar;

				//exception for Channel Islands Display: as this belongs to UK, Channel Islands will be the state
				//however this is too long to be on the same line with postcode ...	 so add breakline
				if(state=="Channel Islands"){
					fullAddress = curr.street + "<br/>"+street2 + curr.city + "<br/> " + state + "<br/> "+postcodeVar;

					
				}
				// Pin store on map
				var infoboxContent = "<div class='store' style='color:black'><h4>" + curr.title + "</h4>" + fullAddress + 
					"<br/><a href='javascript:void(0)' onclick='clickRoute(" + curr.lat + "," + curr.lng + "," + i + ");'>"+directionsText+"</a>" + 
					" | <a href='javascript:void(0)' onclick='setDetails(" + i + ");'>"+detailsText+"</a></div>"; 
				storeMarkers.push(createMarker(infoboxContent, new google.maps.LatLng(curr.lat, curr.lng), (i+1)));
				// Display store in sidebar
				var currPhone=curr.phone!=null?phoneText+' '+curr.phone+'<br/>':'';
				var link=curr.url;
				if(link !=null){
					if(link.length>40)link=link.substring(0,40);
				}
				var currWeb=curr.url!=null?'<br\>Web ' +'  <a class="link" target="_blank" href="'+curr.url+'">'+link+' </a>':'';
				var distanceOrMiles=(locale == 'en_US' ||locale == 'en_GB' )? 'Miles' : 'KM';
				htmlContent += "<div >" + 
					"<a style='float:left' href='javascript:void(0)' onclick='triggerMarker(" + marker + ");'><img src='http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (i+1) + "|FF0040|000000'/></a>" + 
					"<div style='display:table-cell; padding-left:10px;' class='number'><span class='second' style='font-weight:bold'>" + curr.title + "</span><p>" + fullAddress + "<br/>" +
					currPhone + distanceText+" "+ round(curr.distance,2) + distanceOrMiles + currWeb+
					"<br/> <a class='link' href='javascript:void(0)' onclick='clickRoute(" + curr.lat + "," + curr.lng + "," + i + ");'>"+directionsText+"</a> | " + 
					" <a class='link' href='javascript:void(0)' onclick='setDetails(" + i + ");'>"+detailsText+"</a></div>"
					+  "</p></div>";
			}
			// --------------- PAGINATION ------------------
			htmlContent += "<div id='paging'>";
			// Display back button
			if(start > 0) {
				htmlContent += "<a class='link' href='javascript:void(0)' onclick='listStores(" + (start-displayLimit) + ");'>&laquo;"+previousText+"</a> | ";
			}
			// Display page numbers
			var currPage = start/displayLimit+1;
			var pageGroupStart = (Math.ceil(currPage/pageLimit)-1)*pageLimit+1;
			var pageGroupEnd = pageGroupStart + pageLimit - 1;
			if(pageGroupEnd > lastPage)
				pageGroupEnd = lastPage;
			for(; pageGroupStart <= pageGroupEnd; pageGroupStart++) {
				if(pageGroupStart == currPage)
					htmlContent += "<span class='selected'>" + pageGroupStart + "</span>&nbsp;|&nbsp;";
				else
					htmlContent += "<a class='link' href='javascript:void(0)' onclick='listStores(" + ((pageGroupStart-1)*displayLimit) + ");'>" + pageGroupStart +"</a>&nbsp;|&nbsp;";
			}
			
			// Display next button
			if(limit != stores.length) {
				htmlContent += "<a  class='link' href='javascript:void(0)' onclick='listStores(" + limit + ");'>"+nextText+"&raquo;</button>";
			}
			htmlContent += "</div>";
			// --------------- /PAGINATION ------------------
			$("#storeList").html(htmlContent);
			resetView();
		}
		/**
		 * Set the store details div HTML. 
		 */
		function setDetails(index) {
			var curr = stores[index%25];
			var street2=curr.street2!=null?curr.street2+'<br/>':'';
			var state=curr.state!=null?curr.state+' ':'';
			
			var currPhone=curr.phone!=null?phoneText+' '+curr.phone+'<br/>':'';
			var currFax=curr.fax!=null?faxText +' '+curr.fax+'<br/>':'';
			var currEmail=curr.email!=null?'Email' +' '+curr.email+'<br/>':'';
			var currWeb=curr.url!=null?'Web ' +'  <a class="link" target="_blank" href="'+curr.url+'">'+curr.url+' </a>':'';
			var postcodeVar=curr.postalcode!=null?curr.postalcode+' ':'';
			var fullAddress ="";
		          var selCountry=curr.country;
			//scandinavian format
		          if(selCountry =="NO" ||selCountry =="SE" ||selCountry =="FI" ||selCountry =="DK"||selCountry =="DE"||selCountry =="AT"){				
				 fullAddress = curr.street + "<br/>"+street2  + postcodeVar + curr.city + " " + state;
		           }
			else
			fullAddress = curr.street + "<br/>"+street2 + curr.city + " " + state + postcodeVar;
				//exception for Channel Islands Display: as this belongs to UK, Channel Islands will be the state
				//however this is too long to be on the same line with postcode ...	 so add breakline
				if(state=="Channel Islands"){
					fullAddress = curr.street + "<br/>"+street2 + curr.city + "<br/> " + state + "<br/> "+postcodeVar;			
				}
			$("#store_details_content").
			find("#store_details_address").html("<h4>" + (index+1) + ". "+ curr.title + "</h4>" + fullAddress).end().
			find("#store_details_contact").html(
					currPhone +
					 currFax+ 
					 currEmail + 
					 currWeb).end().	
			find("#store_details_directions").unbind().click(function(){clickRoute(curr.lat, curr.lng, index); return false;});
			map.setOptions({center: new google.maps.LatLng(curr.lat, curr.lng), zoom: 16});
			showDetails();
		}
		function showDetails() {
			$("#map_canvas").animate({height: 379});
			$("#store_details").slideDown();
		}

		function hideDetails() {
			$("#map_canvas").animate({height: 425});
			$("#store_details").slideUp();
		}

		// -----------------------------------------------------ROUTING FUNCTIONS----------------------------------------------
		/**
		*  Routes from home to location. If 'e' is a MouseEvent object fired by a Pushpin, then that pushpin is used for 
		*  routing; otherwise, it will route to the store at stores[i].
		*/
		function clickRoute(lat, lng, index) {
			infoWin.close();
			//hideDetails();
			var destination = new google.maps.LatLng(lat, lng);
		    var request = {
		        origin: home.getPosition(), 
		        destination: destination,
		        travelMode: google.maps.DirectionsTravelMode.DRIVING
		    };
		    // Clear map except for home and destination
		    for(var i = 0; i < storeMarkers.length; i++) {
		    	var curr = storeMarkers[i];
		    	if(!curr.getPosition().equals(destination)) 
		    		curr.setMap(null);
		    }
		    // Set directions control values
		    dirControl.find("#destB").val(stores[index].title);
		    dirControl.find("#travelMode").val("DRIVING");
		    if(stores[index].distance > 5)
		    	dirControl.find("#travelMode").hide();
		    else
		    	dirControl.find("#travelMode").show();
		    travelOptions = {destA: home.getPosition(), destB: destination, mode:"DRIVING"};
		    directionsService.route(request, RouteCallback);
		}

		function RouteCallback(results, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		    	// Clear map except for home and destination
		    	if(directionsDisplay.getMap() == null) { // If map was previously cleared (e.g. by new search)
		    		directionsDisplay.setMap(map);
		    		directionsDisplay.setPanel(document.getElementById("dirPanel"));
		    	}
		    	dirControl.show();
		        directionsDisplay.setDirections(results);
		        showDirectionsTab();
			} else {
				$("#statusModule").html(routeErrorText+" " + status);
				$("#statusModule").show();
			}
		}

		function changeMode(mode) {
			if(directionsDisplay.getPanel()) {
				travelOptions.mode = mode;
				var request = {
						origin: travelOptions.destA,
						destination: travelOptions.destB,
						travelMode: google.maps.DirectionsTravelMode[mode]
				};
				directionsService.route(request, RouteCallback);
			}
		}

		function reverseDirections() {
			if(directionsDisplay.getPanel()) {
				// Switch strings in display field
				var destStrA = dirControl.find("#destA");
				var destStrB = dirControl.find("#destB");
				var temp = destStrA.val();
				destStrA.val(destStrB.val());
				destStrB.val(temp);
				// Actually reverse directions
				temp = travelOptions.destA;
				travelOptions.destA = travelOptions.destB;
				travelOptions.destB = temp;
				var request = {
						origin: travelOptions.destA,
						destination: travelOptions.destB,
						travelMode: google.maps.DirectionsTravelMode[travelOptions.mode]
				};
				directionsService.route(request, RouteCallback);
			}	
		}
		//-----------------------------------------------------MISC. FUNCTIONS----------------------------------------------
		/**
		 * Creates a marker on the map at the specified 'position'. If 'index' is set, the pushpin will be labeled with the specified number.
		 * An infoWindow event will be attached to the marker, which will display the specified 'htmlContent'.
		 */
		function createMarker(htmlContent, position, index) {
			var marker = new google.maps.Marker({
				map: map,
				position: position
			});
			if(index) {
				marker.setIcon("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + index + "|FF0040|000000");
				marker.setShadow("http://www.elixirstrings.com:80/locator/resources/shadow.png");
			}
			google.maps.event.addListener(marker, 'click', function(e) {
				infoWin.setContent(htmlContent);
				infoWin.open(map, marker);
			});
			return marker;
		}

		/**
		 * Clear all markers and routes from map
		 */
		function clearMap() {
			clearStoreMarkers();
			clearRoute();
			stores.length = 0;
			google.maps.event.clearInstanceListeners(home);
			home.setMap(null);
			home = null;
		}

		function clearStoreMarkers() {
			for(var i = 0; i < storeMarkers.length; i++) {
				var curr = storeMarkers[i];
				google.maps.event.clearInstanceListeners(curr);
				curr.setMap(null);
			}
			storeMarkers.length = 0;
			infoWin.close();
			hideDetails();
		}

		function clearRoute() {
			directionsDisplay.setMap(null);
			directionsDisplay.setPanel(null);
			dirControl.hide();
		}

		function triggerMarker(index) {
			google.maps.event.trigger(storeMarkers[index], 'click');
		}
		/*
		* Refocuses the map view based on the current markers in 'storeMarkers[]' and 'home'.
		*/
		function resetView() {
			// Construct a rectangle containing current stores and user home
			var bounds = new google.maps.LatLngBounds(home.getPosition(), home.getPosition());
			for(var i = 0; i < storeMarkers.length; i++) {
				var curr = storeMarkers[i];
				curr.setMap(map);
				bounds.extend(curr.getPosition());
			}
			map.fitBounds(bounds);
		}

		function showStoresTab() {
			$("#mapDirections").hide();
			$("#directionsTabButton").removeClass();
			$("#storesTabButton").addClass("selected");
			$("#storeList").show();
		}
		function showDirectionsTab() {
			$("#storeList").hide();
			$("#storesTabButton").removeClass();
			$("#directionsTabButton").addClass("selected");
			$("#mapDirections").show();
		}
		// Takes in a float and rounds it to the decPlace'th decimal place. 
		function round(str, decPlace) {
			var n = parseFloat(str);
			var pwr = Math.pow(10, decPlace);
			return Math.round(n*pwr) / pwr;
		}
		function kilosToMiles(kilos) {
			return kilos*0.621371192;
		}
		function getCountryFromResult(currentResult) { 
			var country="";
			var address_components=currentResult.address_components;
			if(address_components!=null){
				var counter=0;
				for (counter=0; counter<address_components.length; counter++) {
					var addrType=new String(address_components[counter].types);
					var n=addrType.search("country"); 
					if(n>=0){
					country=address_components[counter].short_name;
					}

				}
			}	
			return country;
		}
	