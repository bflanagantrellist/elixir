$(function(){
    var app = {
        init: function(){
        	this.loadFacebook();
        	this.loadTwitter();
        },

	    loadFacebook: function(){
	    	var self = this;
	    	
			$.ajax({
		       	type: 'GET',
		        url: 'http://elixir.trellist-dev.com/api/feeds.php?type=facebook&callback=?',
		        contentType: "application/json",
		        dataType: 'jsonp',
		        success: function(response){		        	
		        	var list = $('<ul></ul>');

		        	var count = 0;
		        	$(response.data).each(function(){
		        		if(count++ < 4){
			        		var params = {
			        			image_url: this.picture || this.icon || 'images/defaultFacebookAvatar.png',
			        			titleText: this.from.name || this.name || this.message.substring(0, 50) + '...',
			        			contentText: this.message,
			        			dateCreated: this.created_time.substr(0, this.created_time.indexOf('T')),
			        			id: this.id,
			        			type: 'facebook'
			        		};

				    		list.append(self.getTemplate(params));
				    	}
		        	});
		        	$('.facebook .results').html(list);
		        }
			});
	    },

	    loadTwitter: function(){
	    	var self = this;

	    	$.ajax({
		       type: 'GET',
		        url: 'http://elixir.trellist-dev.com/api/feeds.php?type=twitter&callback=?',
		        contentType: "application/json",
		        dataType: 'jsonp',
		        success: function(response){
		        	var list = $('<ul></ul>');

		        	var count = 0;
		        	$(response).each(function(){
		        		if(count++ < 5){
			        		var params = {
			        			image_url: this.user.profile_image_url,
			        			titleText: this.text.substr(0, this.text.indexOf(':')),
			        			contentText: this.text.substr(this.text.indexOf(':') + 2),
			        			dateCreated: this.created_at.substr(0, this.created_at.indexOf('+')),
			        			id: this.id,
			        			type: 'twitter'
			        		};

				    		list.append(self.getTemplate(params));
				    	}
		        	});
		        	$('.twitter .results').html(list);
		        }
		    });
	    },

	    getTemplate: function(params){
			var node = $('<li></li>');

			icon = $('<span></span>');
			if(params.image_url){
				icon = $('<img src="' + params.image_url + '"/>');
			}
    		var title = $('<p>' + params.titleText + '<p>').addClass('title');

    		var content = $('<p></p>');
    		content.text(params.contentText);

    		var footer = $('<footer>' + params.dateCreated + '</footer>');

    		var actions = $('<nav class="actions"></nav>');

    		if(params.type === 'twitter'){
    			var respond = $('<a href="https://twitter.com/intent/tweet?in_reply_to?tweet_id=' + params.id + '">Reply</a>').addClass('reply');
	    		var retweet = $('<a href="https://twitter.com/intent/retweet?tweet_id=' + params.id + '">Retweet</a>').addClass('retweet');
    			var favorite = $('<a href="https://twitter.com/intent/favorite?tweet_id=' + params.id + '">Favorite</a>').addClass('favorite');

    			actions.append(respond)
    			       .append(retweet)
    			       .append(favorite);
    		} else {
    			if(params.action){
    				var like = $('<a href="' + params.action.like.url + '">Like</a>');
	    			var comment = $('<a href="' + params.action.comment.url + '">Comment</a>');
    				var share = $('<a href="' + params.action.share.url + '">Share</a>');

    				actions.append(like)
    			   		   .append(comment)
    			       	   .append(share); 
    			}   			    
    		}

			footer.append(actions);

    		node.append(icon)
    			.append(title)
    			.append(content)
    			.append(footer);

    		return node;
	    }
	}

    app.init();
});